"""unit test for PhaseReaction and ComponentContribution."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import numpy as np
import pytest

from equilibrator_api import Q_
from equilibrator_api.phased_compound import (
    AQUEOUS_PHASE_NAME,
    GAS_PHASE_NAME,
    RedoxCarrier,
)


def test_water_balancing(reaction_dict, comp_contribution):
    """Test if the reaction can be balanced using H2O."""
    missing_water = reaction_dict["missing_water"].clone()
    assert not missing_water.is_balanced()
    assert missing_water.is_balanced(ignore_atoms=("H", "O", "e-"))

    balanced_rxn = missing_water.balance_with_compound(
        comp_contribution.get_compound("kegg:C00001"),
        ignore_atoms=("H",),
    )
    assert balanced_rxn is not None


def test_atp_hydrolysis_physiological_dg(reaction_dict, comp_contribution):
    """Test the dG adjustments for physiological conditions (with H2O)."""
    warnings.simplefilter("ignore", ResourceWarning)

    atp_hydrolysis = reaction_dict["atpase"].clone()
    assert atp_hydrolysis.is_balanced()

    phys_dg_corr = atp_hydrolysis.physiological_dg_correction()
    assert phys_dg_corr.m_as("") == pytest.approx(np.log(1e-3), rel=1e-3)

    for compound_id, concentration in [
        ("kegg:C00002", "1uM"),
        ("kegg:C00008", "1uM"),
        ("kegg:C00009", "1uM"),
    ]:
        atp_hydrolysis.set_abundance(
            comp_contribution.get_compound(compound_id),
            Q_(concentration),
        )

    dg_corr = atp_hydrolysis.dg_correction()
    assert dg_corr.m_as("") == pytest.approx(np.log(1e-6), rel=1e-3)


def test_fermentation_gas(reaction_dict):
    """Test the dG adjustments for physiological conditions (in gas phase)."""
    fermentation_gas = reaction_dict["fermentation_gas"].clone()
    assert fermentation_gas.is_balanced()
    assert fermentation_gas.physiological_dg_correction().m_as("") == pytest.approx(
        np.log(1e-9), rel=1e-3
    )


def test_multi(comp_contribution, reaction_dict, allclose):
    """Test the dG estimate for mutli-reactions."""
    reaction_list = [
        reaction_dict["fermentation_gas"],
        reaction_dict["pyruvate_decarboxylase"],
    ]

    (
        standard_dg_prime,
        dg_uncertainty_cov,
    ) = comp_contribution.standard_dg_prime_multi(
        reaction_list,
        uncertainty_representation="cov",
    )

    _, dg_uncertainty_precision = comp_contribution.standard_dg_prime_multi(
        reaction_list,
        uncertainty_representation="precision",
    )

    assert allclose(
        standard_dg_prime.m_as("kJ/mol"), np.array([-214.8, -16.6]), atol=0.1
    )
    assert allclose(
        dg_uncertainty_cov.m_as("kJ**2/mol**2"),
        np.array([[42.5, 20.6], [20.6, 10.7]]),
        atol=0.1,
    )
    assert allclose(
        dg_uncertainty_precision.m_as("mol**2/kJ**2"),
        np.array([[0.32, -0.61], [-0.61, 1.26]]),
        atol=0.01,
    )


def test_atp_hydrolysis_dg(comp_contribution, reaction_dict):
    """Test the CC predictions for ATP hydrolysis."""
    warnings.simplefilter("ignore", ResourceWarning)
    atp_hydrolysis = reaction_dict["atpase"].clone()

    for compound_id, concentration in [
        ("kegg:C00002", "1mM"),
        ("kegg:C00008", "10mM"),
        ("kegg:C00009", "10mM"),
    ]:
        atp_hydrolysis.set_abundance(
            comp_contribution.get_compound(compound_id),
            Q_(concentration),
        )

    standard_dg_prime = comp_contribution.standard_dg_prime(atp_hydrolysis)

    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(-30.1, abs=0.1)
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(0.3, abs=0.1)

    dg_prime = comp_contribution.dg_prime(atp_hydrolysis)
    assert dg_prime.value.m_as("kJ/mol") == pytest.approx(-35.8, abs=0.1)

    physiological_dg_prime = comp_contribution.physiological_dg_prime(atp_hydrolysis)
    assert physiological_dg_prime.value.m_as("kJ/mol") == pytest.approx(-47.2, abs=0.1)


def test_gibbs_energy_pyruvate_decarboxylase(comp_contribution, reaction_dict):
    """Test the CC predictions for pyruvate decarboxylase."""
    warnings.simplefilter("ignore", ResourceWarning)

    reaction = reaction_dict["pyruvate_decarboxylase"].clone()

    assert reaction.is_balanced()

    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(-16.6, abs=0.1)
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(3.3, abs=0.1)


def test_reduction_potential(comp_contribution, reaction_dict):
    """Test the CC predictions for a redox half-reaction."""
    warnings.simplefilter("ignore", ResourceWarning)

    reaction = reaction_dict["oxaloacetate_half"].clone()

    assert reaction.check_half_reaction_balancing() == 2
    assert reaction.is_balanced(ignore_atoms=["H", "e-"])

    e_prime = comp_contribution.e_prime(reaction)
    assert e_prime.value.m_as("mV") == pytest.approx(-175.7, abs=0.1)
    assert e_prime.error.m_as("mV") == pytest.approx(3.3, abs=0.1)

    standard_e_prime = comp_contribution.standard_e_prime(reaction)
    assert standard_e_prime.value.m_as("mV") == pytest.approx(-175.7, abs=0.1)
    assert standard_e_prime.error.m_as("mV") == pytest.approx(3.3, abs=0.1)

    physiological_e_prime = comp_contribution.physiological_e_prime(reaction)
    assert physiological_e_prime.value.m_as("mV") == pytest.approx(-175.7, abs=0.1)
    assert physiological_e_prime.error.m_as("mV") == pytest.approx(3.3, abs=0.1)


def test_unresolved_reactions(comp_contribution, reaction_dict):
    """Test the CC predictions for a reaction that cannot be resolved."""
    reaction = reaction_dict["unresolved"].clone()
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.error.m_as("kJ/mol") > 1e4


def test_generic_compounds(comp_contribution, reaction_dict):
    """Test the CC predictions for reaction that contain generic compounds."""
    reaction = reaction_dict["unresolved"].clone()
    standard_dg = comp_contribution.standard_dg(reaction)
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg.value.m_as("kJ/mol") == pytest.approx(122.6, abs=0.1)
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(79.7, abs=0.1)


def test_stored_formation_energies(comp_contribution, reaction_dict):
    """Test the CC predictions for compounds with stored formation dG'0."""
    reaction = reaction_dict["hydrogen sulfide:NADP+ oxidoreductase"].clone()

    standard_dg = comp_contribution.standard_dg(reaction)
    assert standard_dg.value.m_as("kJ/mol") == pytest.approx(16.84, abs=0.1)

    standard_dg, _ = comp_contribution.standard_dg_multi([reaction])
    assert standard_dg[0].m_as("kJ/mol") == pytest.approx(16.84, abs=0.1)


def test_reversibility_index(comp_contribution, reaction_dict):
    """Test the reversibility index."""
    atp_hydrolysis = reaction_dict["atpase"].clone()
    ln_gamma = comp_contribution.ln_reversibility_index(atp_hydrolysis)
    assert ln_gamma.value.m_as("") == pytest.approx(-12.7, abs=0.1)


def test_reverse(comp_contribution, reaction_dict):
    """Test the reverse function."""
    atp_hydrolysis = reaction_dict["atpase"].clone()
    hash1 = hash(atp_hydrolysis)
    hash2 = hash(atp_hydrolysis.reverse())
    hash3 = hash(atp_hydrolysis.reverse().reverse())
    assert hash1 != hash2
    assert hash1 == hash3

    hash4 = atp_hydrolysis.hash_md5(reversible=False)
    hash5 = atp_hydrolysis.hash_md5(reversible=True)
    hash6 = atp_hydrolysis.reverse().hash_md5(reversible=False)
    hash7 = atp_hydrolysis.reverse().hash_md5(reversible=True)
    assert hash4 != hash6
    assert hash5 == hash7

    assert (
        atp_hydrolysis < atp_hydrolysis.reverse()
        or atp_hydrolysis >= atp_hydrolysis.reverse()
    )


def test_reaction_phases(comp_contribution, reaction_dict):
    """Test the phases and abundance calculations."""
    pyruvate_decarboxylase = reaction_dict["pyruvate_decarboxylase"].clone()
    co2 = comp_contribution.get_compound("kegg:C00011")
    phased_co2, co2_coeff = pyruvate_decarboxylase.get_phased_compound(co2)
    assert co2_coeff == 1
    assert pyruvate_decarboxylase.get_phase(co2) == AQUEOUS_PHASE_NAME
    assert phased_co2.phase == AQUEOUS_PHASE_NAME
    pyruvate_decarboxylase.set_phase(co2, GAS_PHASE_NAME)
    assert pyruvate_decarboxylase.get_phase(co2) == GAS_PHASE_NAME
    assert phased_co2.phase == GAS_PHASE_NAME

    with pytest.raises(ValueError):
        # in gas phase one cannot set abundance in molar
        pyruvate_decarboxylase.set_abundance(co2, Q_("0.1 mM"))

    pyruvate_decarboxylase.set_abundance(co2, Q_("0.1 mbar"))
    assert pyruvate_decarboxylase.get_abundance(co2) == Q_("0.1 mbar")

    pyruvate_decarboxylase.reset_abundances(physiological=False)
    assert pyruvate_decarboxylase.get_abundance(co2) == Q_("1 bar")
    assert not pyruvate_decarboxylase.is_physiological
    pyruvate_decarboxylase.reset_abundances(physiological=True)
    assert pyruvate_decarboxylase.get_abundance(co2) == Q_("1 mbar")
    assert pyruvate_decarboxylase.is_physiological


def test_stoichiometry(comp_contribution, reaction_dict):
    """Test the stoichiometry functions of PhasedReaction."""
    pyruvate_decarboxylase = reaction_dict["pyruvate_decarboxylase"].clone()
    co2 = comp_contribution.get_compound("kegg:C00011")
    assert pyruvate_decarboxylase.get_coeff(co2) == 1
    assert pyruvate_decarboxylase.get_stoichiometry(co2) == 1
    pyruvate_decarboxylase.add_stoichiometry(co2, 2)
    assert pyruvate_decarboxylase.get_stoichiometry(co2) == 3


def test_reaction_with_missing_inchi(comp_contribution):
    """Test a reaction where one of the compounds is missing an InChI."""

    warnings.simplefilter("ignore", UserWarning)
    rxn_str = "CHEBI:69477 + KEGG:C00007 = CHEBI:69478 + CHEBI:44785"
    rxn = comp_contribution.parse_reaction_formula(rxn_str)
    phys_dg_prime = comp_contribution.physiological_dg_prime(rxn)
    assert phys_dg_prime.error.m_as("kJ/mol") > 1e4


def test_protons(comp_contribution, reaction_dict):
    """Test the units and balancing of protons."""
    proton_cpd = comp_contribution.get_compound("kegg:C00080")
    assert reaction_dict["protons"].is_balanced()
    with pytest.raises(ValueError):
        reaction_dict["protons"].set_abundance(proton_cpd, Q_(200, "mM"))


def test_electrons(comp_contribution, reaction_dict):
    """Test the units and balancing of redox carriers."""
    electron_cpd = comp_contribution.get_compound("kegg:C05359")
    redox_carrier = RedoxCarrier(
        electron_cpd,
        potential=Q_(100.0, "mV"),
    )
    assert redox_carrier.abundance.check("[energy]/[charge]")

    with pytest.raises(ValueError):
        redox_carrier = RedoxCarrier(
            electron_cpd,
            potential=Q_(100.0, "mM"),
        )
    assert not reaction_dict["oxaloacetate_half"].is_balanced()
    assert reaction_dict["oxaloacetate_half"].check_half_reaction_balancing() == 2
    assert reaction_dict["redox"].is_balanced()
    assert reaction_dict["redox"].check_half_reaction_balancing() == 0
    with pytest.raises(ValueError):
        reaction_dict["redox"].set_abundance(redox_carrier, Q_(200, "mM"))


@pytest.mark.parametrize(
    "redox_potential, exp_standard_dg_prime",
    [
        (Q_("-200 mV"), Q_("-4.7 kJ/mol")),
        (Q_("0 mV"), Q_("33.9 kJ/mol")),
        (Q_("200 mV"), Q_("72.5 kJ/mol")),
    ],
)
def test_redox_reaction(
    redox_potential, exp_standard_dg_prime, comp_contribution, reaction_dict
):
    """Test the use of unspecific redox carriers in ΔG' esimates."""
    electron_cpd = comp_contribution.get_compound("metanetx.chemical:MNXM861")
    redox_reaction = reaction_dict["redox"].clone()

    redox_reaction.set_abundance(electron_cpd, redox_potential)
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction_dict["redox"])
    dg_prime = comp_contribution.standard_dg_prime(reaction_dict["redox"])

    # changing the E'0 should not affect the ΔG' of the reaction (i.e. it is
    # not equivalent to changing the abundance of one of the reactants).
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime.m_as("kJ/mol"), abs=0.1
    )
    assert dg_prime.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime.m_as("kJ/mol"), abs=0.1
    )


def test_sensitivity_to_p_h(comp_contribution, reaction_dict):
    """Test the calculation of the deriviative w.r.t. pH."""

    atp_hydrolysis = reaction_dict["atpase"]

    dp_h = 0.01
    for p_h in [6.0, 7.0, 8.0]:
        # calculate sensitivity by increasing the pH a bit and taking the
        # difference then, compare it to the analytical calculation made by
        # the function dg_prime_sensitivity_to_p_h().
        comp_contribution.p_h = Q_(p_h)
        dg_prime1 = comp_contribution.standard_dg_prime(atp_hydrolysis)
        comp_contribution.p_h = Q_(p_h + dp_h)
        dg_prime2 = comp_contribution.standard_dg_prime(atp_hydrolysis)
        derivative = (dg_prime2 - dg_prime1).value.m_as("kJ/mol") / dp_h

        sensitivity_to_p_h = comp_contribution.dg_prime_sensitivity_to_p_h(
            atp_hydrolysis
        )

        assert sensitivity_to_p_h.m_as("kJ/mol") == pytest.approx(derivative, abs=0.1)


def test_analysis_functions(comp_contribution, reaction_dict):
    """Test the analysis functions."""
    assert comp_contribution.is_using_group_contribution(
        reaction_dict["benzoyl_coa_hydratase"]
    )
    assert not comp_contribution.is_using_group_contribution(reaction_dict["atpase"])

    # atp hydrolysis has a lot of relevant sample data in TECRDB
    dg_analysis = list(comp_contribution.dg_analysis(reaction_dict["atpase"]))
    assert len(dg_analysis) == 2939
