"""unit test for PhaseReaction and ComponentContribution."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import pytest

from equilibrator_api import Q_


@pytest.mark.parametrize(
    "compound_id, exp_standard_dg_prime",
    [
        ("kegg:C00031", Q_("-2927.8 kJ/mol")),
        ("kegg:C00033", Q_("-871.1 kJ/mol")),
        ("kegg:C00064", Q_("-2040.4 kJ/mol")),
    ],
)
def test_oxidation_mg(compound_id, exp_standard_dg_prime, comp_contribution):
    """Test how reactions are balanced by oxidation (with Mg2+).

    Include Mg2+ effects in the training procedure.
    """
    compound = comp_contribution.get_compound(compound_id)
    r1 = comp_contribution.get_oxidation_reaction(compound)
    dg_prime1 = comp_contribution.dg_prime(r1)
    assert dg_prime1.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime.m_as("kJ/mol"), abs=0.1
    )

    reaction = comp_contribution.parse_reaction_formula(f"{compound_id} = ")
    r2 = comp_contribution.balance_by_oxidation(reaction)
    dg_prime2 = comp_contribution.dg_prime(r2)
    assert dg_prime2.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime.m_as("kJ/mol"), abs=0.1
    )


def test_atp_hydrolysis_dg(comp_contribution, reaction_dict):
    """Test the CC predictions for ATP hydrolysis.

    Include Mg2+ effects in the training procedure.
    """
    warnings.simplefilter("ignore", ResourceWarning)
    atp_hydrolysis = reaction_dict["atpase"]

    for compound_id, concentration in [
        ("kegg:C00002", "1mM"),
        ("kegg:C00008", "10mM"),
        ("kegg:C00009", "10mM"),
    ]:
        atp_hydrolysis.set_abundance(
            comp_contribution.get_compound(compound_id), Q_(concentration)
        )

    standard_dg_prime = comp_contribution.standard_dg_prime(atp_hydrolysis)

    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(-30.1, abs=0.1)
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(0.3, abs=0.1)

    dg_prime = comp_contribution.dg_prime(atp_hydrolysis)
    assert dg_prime.value.m_as("kJ/mol") == pytest.approx(-35.8, abs=0.1)

    physiological_dg_prime = comp_contribution.physiological_dg_prime(atp_hydrolysis)
    assert physiological_dg_prime.value.m_as("kJ/mol") == pytest.approx(-47.2, abs=0.1)


def test_gibbs_energy_pyruvate_decarboxylase(comp_contribution):
    """Test the CC predictions for pyruvate decarboxylase.

    Include Mg2+ effects in the training procedure.
    """
    warnings.simplefilter("ignore", ResourceWarning)

    # pyruvate = acetaldehyde + CO2
    formula = "kegg:C00022 = kegg:C00084 + kegg:C00011"
    reaction = comp_contribution.parse_reaction_formula(formula)

    assert reaction.is_balanced()

    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(-16.6, abs=0.1)
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(3.3, abs=0.1)


def test_reduction_potential(comp_contribution):
    """Test the CC predictions for a redox half-reaction.

    Include Mg2+ effects in the training procedure.
    """
    warnings.simplefilter("ignore", ResourceWarning)

    # oxaloacetate = malate
    formula = "kegg:C00036 = kegg:C00149"
    reaction = comp_contribution.parse_reaction_formula(formula)

    assert reaction.check_half_reaction_balancing() == 2
    assert reaction.is_balanced(ignore_atoms=["H", "e-"])

    standard_e_prime = comp_contribution.standard_e_prime(reaction)

    assert standard_e_prime.value.m_as("mV") == pytest.approx(-175.7, abs=1.0)
    assert standard_e_prime.error.m_as("mV") == pytest.approx(3.2, abs=1.0)


def test_unresolved_reactions(comp_contribution):
    """Test the CC predictions for a reaction that cannot be resolved.

    Include Mg2+ effects in the training procedure.
    """
    formula = "kegg:C09844 + kegg:C00003 + kegg:C00001 => " "kegg:C03092 + kegg:C00004"

    reaction = comp_contribution.parse_reaction_formula(formula)
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.error.magnitude > 1e4


def test_reversibility_index(comp_contribution, reaction_dict):
    """Test the reversibility index.

    Include Mg2+ effects in the training procedure.
    """
    atp_hydrolysis = reaction_dict["atpase"]

    ln_gamma = comp_contribution.ln_reversibility_index(atp_hydrolysis)
    assert ln_gamma.value.magnitude == pytest.approx(-12.71, abs=0.1)
