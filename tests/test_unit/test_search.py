"""unit test for PhaseReaction and ComponentContribution."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest


@pytest.mark.parametrize(
    "identifier, inchi, inchi_key",
    [
        (
            "kegg:C00002",
            "InChI=1S/C10H16N5O13P3/c11-8-5-9(13-2-12-8)15(3-14-5)10-7(17)6("
            "16)4(26-10)1-25-30(21,22)28-31(23,24)27-29(18,19)20/h2-4,6-7,10,"
            "16-17H,1H2,(H,21,22)(H,23,24)(H2,11,12,13)(H2,18,19,20)/p-4/t4-,6-"
            ",7-,10-/m1/s1",
            "ZKHQWZAMYRWXGA-KQYNXXCUSA-J",
        ),
        (
            "metanetx.chemical:MNXM7",
            "InChI=1S/C10H15N5O10P2/c11-8-5-9(13-2-12-8)15(3-14-5)10-7(17)6("
            "16)4(24-10)1-23-27(21,22)25-26(18,19)20/h2-4,6-7,10,16-17H,1H2,"
            "(H,21,22)(H2,11,12,13)(H2,18,19,20)/p-3/t4-,6-,7-,10-/m1/s1",
            "XTWYTFMLZFPYCI-KQYNXXCUSA-K",
        ),
    ],
)
def test_get_compound(identifier: str, inchi: str, inchi_key: str, comp_contribution):
    """Test get_compound."""
    assert comp_contribution.get_compound(
        identifier
    ) == comp_contribution.get_compound_by_inchi(inchi)

    assert [
        comp_contribution.get_compound(identifier)
    ] == comp_contribution.search_compound_by_inchi_key(inchi_key)


@pytest.mark.parametrize(
    "name, identifier",
    [
        ("ATP", "kegg:C00002"),
        # ("Adenosine-5'-diphosphate", "bigg.metabolite:adp"),
        # ("water", "CHEBI:15377"),
        # ("H+", "kegg:C00080"),
        # ("ethanol", "kegg:C00469"),
        # ("alpha-D-glucose", "metanetx.chemical:MNXM41"),
        # ("alpha-D-Glucose", "metanetx.chemical:MNXM41"),
        # ("beta-D-glucose", "metanetx.chemical:MNXM105"),
        # ("beta-D-Glucose", "metanetx.chemical:MNXM105"),
        # ("glucose 6-phosphate", "kegg:C00092"),
        # ("D-glucose 6-phosphate", "kegg:C00092"),
    ],
)
def test_search_compound(name: str, identifier: str, comp_contribution):
    """Test search compound (first hit)."""
    cpd1 = comp_contribution.search_compound(name)
    cpd2 = comp_contribution.get_compound(identifier)
    assert cpd1 == cpd2, f"Search compound result does not match reference: {cpd1}"


@pytest.mark.parametrize(
    "text_formula, id_formula",
    [
        (
            "alpha-D-glucose + ATP <=> glucose 6-phosphate + ADP",
            "kegg:C00031 + kegg:C00002 = kegg:C00092 + kegg:C00008",
        ),
        # (
        #     "ATP + H2O = Adenosine-5'-diphosphate + phosphate",
        #     "kegg:C00002 + kegg:C00001 = kegg:C00008 + kegg:C00009",
        # ),
        # (
        #     "alpha-D-glucose = 2 ethanol + 2 CO2",
        #     "metanetx.chemical:MNXM41 = 2 metanetx.chemical:MNXM13 + 2 "
        #     "metanetx.chemical:MNXM303",
        # ),
        # (
        #     "beta-D-glucose = 2 ethanol + 2 CO2",
        #     "metanetx.chemical:MNXM105 = 2 metanetx.chemical:MNXM13 + 2 "
        #     "metanetx.chemical:MNXM303",
        # ),
        # (
        #     "O2 + NADH = NAD+ + water",
        #     "kegg:C00007 + kegg:C00004 = kegg:C00003 + kegg:C00001",
        # ),
    ],
)
def test_search_reaction(text_formula: str, id_formula: str, comp_contribution):
    """Test parsing of freetext reaction formulas."""
    rxn1 = comp_contribution.search_reaction(text_formula)
    rxn2 = comp_contribution.parse_reaction_formula(id_formula)
    assert rxn1 == rxn2, f"Search reaction result does not match reference: {rxn1}"
