"""A test module for models."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
from path import Path

from equilibrator_api import Q_
from equilibrator_api.model import Bounds, StoichiometricModel


@pytest.fixture(scope="module")
def test_dir() -> Path:
    """Get the test directory."""
    return Path(__file__).abspath().parent


def test_from_network_sbtab(test_dir, comp_contribution):
    """Test generating model from text-based reactions."""
    pathway_filename = str(test_dir / "example_fermentation.tsv")
    model = StoichiometricModel.from_network_sbtab(
        pathway_filename, comp_contrib=comp_contribution
    )

    # test writing results to SBtab without errors
    _ = model.to_sbtab().to_str()

    expected_filename = str(test_dir / "expected_fermentation.tsv")
    expected_model = StoichiometricModel.from_sbtab(
        expected_filename, comp_contrib=comp_contribution
    )

    assert set(model.compound_ids) == set(expected_model.compound_ids)


@pytest.mark.parametrize(
    "sbtab_fname",
    [("ccm_without_dgs.tsv"), ("ccm_with_dgs.tsv"), ("ccm_with_keq.tsv")],
)
def test_pathway(sbtab_fname, test_dir, comp_contribution):
    """Test reading a StoichiometricModel from SBtab."""
    filename = str(test_dir / sbtab_fname)

    model = StoichiometricModel.from_sbtab(filename, comp_contrib=comp_contribution)

    assert model.Nr == 30
    assert model.Nc == 40
    assert model.reaction_df.shape == (30, 3)
    assert model.compound_df.shape == (40, 3)
    assert model.bound_df.shape == (40, 3)
    assert model.ln_conc_lb.shape == (40,)
    assert model.ln_conc_ub.shape == (40,)
    assert model.ln_conc_mu.shape == (40,)
    assert model.ln_conc_sigma.shape == (40,)

    # test that formulas are printed in a nice way
    formulas = list(model.reaction_formulas)
    assert (
        formulas[0] == "D_Glucose + Phosphoenolpyruvate = "
        "D_Glucose_6_phosphate + Pyruvate"
    )

    # try changing one of the bounds
    model.set_bounds("Phosphoenolpyruvate", Q_(5.0, "mM"), Q_(10.0, "mM"))
    assert model.get_bounds("Phosphoenolpyruvate") == (
        Q_(5.0, "mM"),
        Q_(10.0, "mM"),
    )

    model.set_bounds("Phosphoenolpyruvate", None, Q_(11.0, "mM"))
    assert model.get_bounds("Phosphoenolpyruvate") == (
        Q_(5.0, "mM"),
        Q_(11.0, "mM"),
    )

    model.set_bounds("Phosphoenolpyruvate", Q_(1.0, "uM"), None)
    assert model.get_bounds("Phosphoenolpyruvate") == (
        Q_(1.0, "uM"),
        Q_(11.0, "mM"),
    )

    with pytest.raises(ValueError):
        model.set_bounds("Phosphoenolpyruvate", Q_(0.1, "kilogram"), None)

    with pytest.raises(ValueError):
        model.set_bounds("Phosphoenolpyruvate", None, Q_(298.0, "kelvin"))

    with pytest.raises(KeyError):
        model.set_bounds("non_existing_compound_id", Q_(5.0, "mM"), Q_(10.0, "mM"))

    with pytest.raises(KeyError):
        model.get_bounds("non_existing_compound_id")


def test_default_bounds(comp_contribution):
    """Test the default bounds."""
    bounds = Bounds.get_default_bounds(comp_contribution)

    bounds.check_bounds()
